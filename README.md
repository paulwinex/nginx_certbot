### Start

1. Create and edit `.env_local` file

2. Execute
    
    `docker-compose up -d`
    
In first time will be created nginx config file using variables from `.env` and `.env_local`

3. Execute

    `docker exec -it nginx_certbot certbot`
    
    and create new certificate using certbot's wizard
    
### Renew

Renew process started every day via cron
    