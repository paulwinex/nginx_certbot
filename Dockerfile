FROM ubuntu:16.04
EXPOSE 80
EXPOSE 443
RUN apt-get update && \
    apt-get install -y software-properties-common mc cron nginx && \
    add-apt-repository -y ppa:certbot/certbot && \
    apt-get update && \
    apt-get install -y python python-dev libffi6 libffi-dev libssl-dev curl build-essential python-certbot-nginx
COPY ./scripts /scripts/
RUN (crontab -l 2>/dev/null; echo "0 0 * * * root /usr/bin/certbot renew") | crontab -
ENTRYPOINT ["/scripts/entrypoint.sh"]
