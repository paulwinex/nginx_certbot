#!/usr/bin/env bash

# create scripts if not exists
python /scripts/startup.py
# start nginx
nginx -g 'daemon off;'