import os
import sys

sites_enabled = '/etc/nginx/sites-enabled'

domain = os.getenv('PROJECT_HOST')
if not domain:
    print('DOMAIN NOT DEFINED')
    sys.exit()

conf = os.path.join(sites_enabled, domain)
if os.path.exists(conf):
    print('CONFIG ALREADY EXISTS')
    sys.exit()

template = '/scripts/template.conf'
if not os.path.exists(template):
    template = '/scripts/template_example.conf'
if not os.path.exists(template):
    print('TEMPLATE NOT EXISTS', template)
text = open(template).read()
new_conf = text.format(
    DOMAIN=domain,
    APP_PORT=os.getenv('APP_PORT', 8000)
)
open(conf, 'w').write(new_conf)

print('CONFIG CREATED')

if not os.listdir('/etc/letsencrypt'):
    print('Start "certbot" to create certificate:')
    print('   docker exec -it <CONTAINER> certbot')
