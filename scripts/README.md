## Create new cert

`docker exec nginx cerbot --nginx`

## Manually renew cert

`docker exec nginx cerbot renew`

## Test Server 

`docker exec nginx python3 -m http.server 8000`


